from flask import Flask, request
import openai
import json
import time
import socket
import re
import math
import multiprocessing as mp
from datetime import datetime, timezone
import multiprocessing.queues as mpq
import functools
import dill
import requests
from bs4 import BeautifulSoup
import torch
import http.client
from flask_cors import CORS
from selenium import webdriver
from bs4 import BeautifulSoup
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from transformers import AutoTokenizer
from transformers import AutoModelForQuestionAnswering, AutoTokenizer, pipeline

from typing import Tuple, Callable, Dict, Optional, Iterable, List, Any



openai.api_key = "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"
tokenizer = AutoTokenizer.from_pretrained("gpt2")
model_name = "deepset/roberta-base-squad2"

# a) Get predictions
nlp = pipeline('question-answering', model=model_name, tokenizer=model_name)
app = Flask(__name__)

CORS(app)

cors = CORS(app, resource={
    r"/*":{
        "origins":"*"
    }
})

# Class to apply timeout for ChatGPT api call
class TimeoutError(Exception):

    def __init__(self, func: Callable, timeout: int):
        self.t = timeout
        self.fname = func.__name__

    def __str__(self):
            return f"function '{self.fname}' timed out after {self.t}s"


def _lemmiwinks(func: Callable, args: Tuple, kwargs: Dict[str, Any], q: mp.Queue):
    """lemmiwinks crawls into the unknown"""
    q.put(dill.loads(func)(*args, **kwargs))


def killer_call(func: Callable = None, timeout: int = 10) -> Callable:
    """
    Single function call with a timeout

    Args:
        func: the function
        timeout: The timeout in seconds
    """

    if not isinstance(timeout, int):
        raise ValueError(f'timeout needs to be an int. Got: {timeout}')

    if func is None:
        return functools.partial(killer_call, timeout=timeout)

    @functools.wraps(killer_call)
    def _inners(*args, **kwargs) -> Any:
        q_worker = mp.Queue()
        proc = mp.Process(target=_lemmiwinks, args=(dill.dumps(func), args, kwargs, q_worker))
        proc.start()
        try:
            return q_worker.get(timeout=timeout)
        except mpq.Empty:
            raise TimeoutError(func, timeout)
        finally:
            try:
                proc.terminate()
            except:
                pass
    return _inners


def discount(list_price, sale_price):
        """
        Calculates the discount amount for a given list price and sale price.

        Args:
        list_price: The list price of the item.
        sale_price: The sale price of the item.

        Returns:
        The discount amount.
        """

        list_price = int(list_price)
        sale_price = int(sale_price)
        if sale_price == 0:
            discount_amount = "N/A"
        elif list_price == 0:
            discount_amount = "N/A"
        else:
            discount_amount = list_price - sale_price
        #   discount_rate = discount_amount / list_price * 100
        #   rounded_value = math.floor(discount_rate * 10**2) / 10**2
        return discount_amount


def driversetup(url):

    try:
        start = time.time()
        response = requests.get(
            url='https://proxy.scrapeops.io/v1/',
            params={
            'api_key': '642910a7-020d-425c-985b-5e671af549ee',
            'url': f'{url}', 
            'render_js': 'true', 
            'residential': 'true', 
            'country': 'us', 
        },
        )
        print(f'url getting time ---------------{time.time()-start}')
        # print('Response Body: ', response.content)
        soup = BeautifulSoup(response.content, 'html.parser')
        # print(soup.text)
        text_data = soup.get_text().strip()
        return text_data
    
    except Exception as e:
            print(e)
            return "no data"



def driversetup2(url):
    options = webdriver.ChromeOptions()
    # run Selenium in headless mode
    options.add_argument('--headless=new')
    options.add_argument('--no-sandbox')
    # overcome limited resource problems
    options.add_argument('--disable-dev-shm-usage')
    options.add_argument("lang=en")
    # open Browser in maximized mode
    options.add_argument("start-maximized")
    # disable infobars
    options.add_argument("disable-infobars")
    # disable extension
    # options.add_argument("--disable-extensions")
    # options.add_argument("--incognito")
    options.add_argument("--disable-javascript")
    options.add_argument("--disable-blink-features=AutomationControlled")
    try:
        driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=options)
        # driver = webdriver.Chrome(options=options)

        # driver.execute_script("Object.defineProperty(navigator, 'webdriver', {get: () => undefined});")
        start = time.time()
        driver.get(url)
        print(f'url getting time ---------------{time.time()-start}')
        element = driver.find_element(By.XPATH,'//body')
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        # print(soup.text)
        text_data = element.text.strip()
        # print(text_data)
        driver.close()

        return text_data
    except Exception as e:
        print(e)
        return "no data"
# def update_chat(messages, role, content):
#     messages.append({'role': role, 'content': content})
#     return messages

def get_price(value):
        pattern = r"[0-9,]+"
        price = re.findall(pattern, value)
        if len(price) == 0:
            return 0
        else:
            price = price[0]
            pattern = r","
            price = re.sub(pattern, "", price)
            return price



def update_chat(messages, role, content):
    new_message = {'role': role, 'content': content}
    if new_message not in messages:
        messages.append(new_message)
    return messages


@killer_call(timeout=30)
def get_chatgpt_response(messages):
    try:
        response = openai.ChatCompletion.create(
            model='gpt-3.5-turbo',
            messages = messages,
            temperature = 0
            # timeout=30.0
        )
        return response['choices'][0]['message']['content']

    except openai.error.APIError as e:
            #Handle API error here, e.g. retry or log
        return print(f"OpenAI API returned an API Error: {e}")
    except openai.error.APIConnectionError as e:
        #Handle connection error here
        return print(f"Failed to connect to OpenAI API: {e}")
        
    except openai.error.RateLimitError as e:
        #Handle rate limit error (we recommend using exponential backoff)
        return print(f"OpenAI API request exceeded rate limit: {e}")
        
    except openai.error.Timeout as e:
        return print(f"OpenAI API request timeout errror: {e}")

def parsing_data(text_data, query_data, messages=None):

    input_ids = tokenizer.encode(text_data, add_special_tokens=True, max_length=4000, truncation=True)
        # num_tokens = len(input_ids)

    first_3900_tokens = input_ids[:3900]
    decoded_text = tokenizer.decode(first_3900_tokens)
    request_data = ' : '.join((f'I want your assistant to find the price from the below given data, give me the just price {query_data}', f'{decoded_text}'))
    messages = [
        {'role':'user', 'content': f'{request_data}'}
    ]
    
    # messages = update_chat(messages, 'user', request_data)
    start_time = time.time()
    model_response = get_chatgpt_response(messages)
    end_time = time.time()
    # response_time = end_time - start_time
    #messages = update_chat(messages, 'assistant', model_response)
    # utc_time = datetime.utcnow()
    QA_input = {
        'question': f'What is the price of the {query_data}',
        'context': f'{model_response}'
    }
    res = nlp(QA_input)
    if 'answer' in res and isinstance(res['answer'], (int, float,str)):
        
        # Remove non-alphabetical characters
        text_alpha = re.sub(r'[^a-zA-Z]', '', res['answer'])

        # Check the length of the resulting string
        if len(text_alpha) > 4:
            new_value = "not found"
        else: 
            new_value = res['answer']

    else:
        new_value = "not found"
    return new_value
# if the incomming request have the raw json format as input, then the POST route is use
@app.route('/chatgpt', methods=['POST'])
def post_req():

    base_url = request.form['base_url']
    text_url = request.form['data']

    base_data = driversetup(base_url)
    text_data = driversetup2(text_url)
   
    query = request.form['query']
    base_response = parsing_data(base_data, query)
    external_resource = parsing_data(text_data, query)

    base = get_price(base_response)
    external = get_price(external_resource)

    discount_rate = discount(base,external)

    return {"Price ": external_resource, "Discount": discount_rate}


if __name__ == '__main__':

    #socket.setdefaulttimeout(10)
    app.run(host= "0.0.0.0", debug=True, port=5000)
